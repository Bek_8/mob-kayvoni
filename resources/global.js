import images from "./images";
import colors from "./colors";
import fonts from "./fonts";
import strings from "./strings";
const global = {
  images,
  colors,
  fonts,
  strings,
};

export default global;
