import { Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
const strings = {
  name: "Kayvoni",
  width: width,
  height: height,
};

export default strings;
