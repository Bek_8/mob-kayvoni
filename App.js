import React, { useState, useEffect, useRef } from "react";
import { Image, Text, TextInput, View } from "react-native";
import * as Font from "expo-font";
import AppContainer from "./src/navigation";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import { store, persistor } from "./src/redux/rootConfig";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { getUser } from "./src/redux/actions/userAction";
import global from "./resources/global";
import { updateUser } from "./src/redux/actions/authAction";
// import * as SplashScreen from "expo-splash-screen";
// import { Asset } from "expo-asset";
import AnimatedSplash from "react-native-animated-splash-screen";
import "./resources/i18n";
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  const notificationListener = useRef();
  const responseListener = useRef();
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  const [loaded, setLoaded] = useState(false);
  const dispatch = store.dispatch;
  const getState = store.getState();
  const { token } = getState.auth;
  const { user } = getState.user;
  const getUserData = () => {
    if (token) {
      dispatch(getUser(token));
      if (user) {
        registerForPushNotificationsAsync(token, user.notification_token);
      }
    }
  };
  const registerForPushNotificationsAsync = async (userToken, settedToken) => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        // alert("Failed to get push token for push notification!");
        return;
      }
      const notificationToken = await Notifications.getExpoPushTokenAsync();
      if (userToken) {
        if (notificationToken != settedToken) {
          const body = {
            notification_token: notificationToken.data,
          };
          dispatch(updateUser(body, userToken));
        }
      }
      if (Platform.OS === "android") {
        Notifications.setNotificationChannelAsync("default", {
          name: "kayvoni",
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: global.colors.main,
        });
      }
    }
  };
  useEffect(() => {
    Font.loadAsync({
      Bold: require("./assets/fonts/bold.ttf"),
      Book: require("./assets/fonts/book.ttf"),
      SemiBold: require("./assets/fonts/semibold.ttf"),
    }).then(() => {
      getUserData();
      setLoaded(true);
    });
  }, []);
  // if (!loaded)
  //   return (
  //     <AnimatedSplash
  //       translucent={true}
  //       isLoaded={loaded}
  //       logoImage={global.images.loaderMain}
  //       backgroundColor={"#262626"}
  //       logoHeight={300}
  //       logoWidth={300}
  //     >
  //       <Image source={global.images.loaderMain} />
  //     </AnimatedSplash>
  //   );
  return (
    <AnimatedSplash
      translucent={true}
      isLoaded={loaded}
      logoImage={global.images.loaderMain}
      backgroundColor={global.colors.main}
      logoHeight={300}
      logoWidth={300}
    >
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          {loaded && <AppContainer />}
        </PersistGate>
      </Provider>
    </AnimatedSplash>
  );
}
