import React from "react";
import { View, TouchableOpacity } from "react-native";
import ArrowBack from "../../assets/img/arrow-back.svg";
import CustomStatusBar from "./CustomStatusBar";
import { useNavigation } from "@react-navigation/native";
import { HeaderWrapp } from "../../resources/styles";

export default ({}) => {
  const navigation = useNavigation();

  return (
    <>
      {/* <CustomStatusBar /> */}
      <View>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <ArrowBack />
        </TouchableOpacity>
      </View>
    </>
  );
};
