import React from "react";
import { SemiBold14 } from "../../resources/palettes";
import Checked from "../../assets/img/checked.svg";
import { CheckBtn } from "../../resources/styles";

export default function CheckButton({ name, color, nameColor, onPress }) {
  return (
    <CheckBtn
      color={color}
      style={{ paddingHorizontal: 10 }}
      onPress={() => onPress()}
    >
      <SemiBold14 color={nameColor}>{name}</SemiBold14>
      <Checked />
    </CheckBtn>
  );
}
