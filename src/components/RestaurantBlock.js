import React from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";
import { Bold14, Book12, Book10 } from "../../resources/palettes";
import Star from "../../assets/img/star.svg";
import Clock from "../../assets/img/clock.svg";
import { useNavigation } from "@react-navigation/native";
import { Restaurant, RestHead, RestBody } from "../../resources/styles";
import EmptyStar from "../../assets/img/mini-empty-star.svg";
import { useTranslation } from "react-i18next";

export default function RestaurantBlock({ restaurant }) {
  const navigation = useNavigation();
  const { name, logo, workdays, rating, sorts } = restaurant;
  const weekday = new Date().getDay();
  const { i18n } = useTranslation();
  const current_lang = i18n.language;

  const arr = [1, 2, 3, 4, 5];
  const ratingArray = arr.slice(0, rating.avg);
  const emptyArray = arr.slice(0, -ratingArray.length);
  return (
    <Restaurant
      onPress={() => navigation.navigate("Restaurant", { id: restaurant.id })}
    >
      <RestHead>
        <Image
          style={{
            width: 74,
            height: 74,
            borderRadius: 50,
            overflow: "hidden",
            resizeMode: "cover",
          }}
          source={{ uri: logo }}
        />
      </RestHead>
      <RestBody>
        <Bold14 color={global.colors.textColor}>{name}</Bold14>
        <Book12 style={{ marginTop: 5 }} color={global.colors.textColor2}>
          {sorts && sorts[0] && sorts[0][`name_${current_lang}`]}
        </Book12>
        <View style={{ marginBottom: 10 }}>
          <View
            style={{
              flexDirection: "row",
              marginTop: 15,
              alignItems: "center",
            }}
          >
            {!!ratingArray.length &&
              ratingArray.map((i) => (
                <Star key={i} style={{ marginRight: 5 }} />
              ))}
            {!!emptyArray.length &&
              emptyArray.map((i) => (
                <EmptyStar key={i} style={{ marginRight: 5 }} />
              ))}
            <Book10 style={{ marginLeft: 5 }} color={global.colors.textColor}>
              ({rating.count})
            </Book10>
          </View>
          <View
            style={{
              flexDirection: "row",
              marginTop: 5,
              alignItems: "center",
            }}
          >
            <Clock />
            <Book10 style={{ marginLeft: 5 }} color={global.colors.textColor}>
              {workdays[weekday].open_time.slice(0, -3)}-
              {workdays[weekday].close_time.slice(0, -3)}
            </Book10>
          </View>
        </View>
      </RestBody>
    </Restaurant>
  );
}
