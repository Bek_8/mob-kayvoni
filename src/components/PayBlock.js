import React from "react";
import { Image, TouchableOpacity } from "react-native";

export default function PayBlock({ image, action = () => null }) {
  return (
    <TouchableOpacity onPress={action}>
      <Image style={{ width: 145, height: 145 }} source={image} />
    </TouchableOpacity>
  );
}
