import React from "react";
import { View } from "react-native";
import global from "../../resources/global";
import { SemiBold14, Book14 } from "../../resources/palettes";
import ArrowRight from "../../assets/img/arrowRight.svg";
import { useNavigation } from "@react-navigation/native";
import { FilterBtn } from "../../resources/styles";
import { useTranslation } from "react-i18next";

export default function FilterCatButton({
  title,
  name,
  choiceType = "category",
  setter = () => null,
  chosenData = [],
}) {
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  const navigation = useNavigation();
  const isChosen = chosenData.length > 0;
  return (
    <View style={{ marginTop: 5 }}>
      <SemiBold14 color={global.colors.main}>{title}</SemiBold14>
      <FilterBtn
        onPress={() =>
          navigation.navigate("SelectScreen", {
            choiceType,
            setter,
            chosenData,
          })
        }
        style={{ paddingHorizontal: 15 }}
      >
        <View style={{ flex: 1, overflow: "hidden" }}>
          <Book14
            color={
              !isChosen ? global.colors.textColor2 : global.colors.textColor
            }
          >
            {isChosen
              ? chosenData.map(
                  (item, key) =>
                    `${item[`name_${current_lang}`]}${
                      key !== chosenData.length - 1 ? ", " : ""
                    }`
                )
              : name}
          </Book14>
        </View>

        <ArrowRight />
      </FilterBtn>
    </View>
  );
}
