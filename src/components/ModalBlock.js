import React, { useRef, useEffect } from "react";
import { Animated, Easing, View } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import { Bold14 } from "../../resources/palettes";

export default function ModalBlock({ title }) {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 4,
      duration: 1200,
      useNativeDriver: true,
      easing: Easing.linear,
    }).start();
  }, [fadeAnim]);
  const position = fadeAnim.interpolate({
    inputRange: [0, 1, 2, 3, 4],
    outputRange: [100, 0, 0, 0, 100],
  });

  return (
    <Animated.View
      style={{
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        width: "100%",
        height: 100,
        backgroundColor: global.colors.lightGreen,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        transform: [{ translateY: position }],
      }}
    >
      <Bold14>{title}</Bold14>
    </Animated.View>
  );
}

const ModalWrapp = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100px;
  background-color: #ff7800;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
`;
