import React from "react";
import { Image, View } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import { Bold14, Book12 } from "../../resources/palettes";
import Clock from "../../assets/img/clock.svg";
import { useNavigation } from "@react-navigation/native";
import { useTranslation } from "react-i18next";

export default function MapRestBlock({ restaurant }) {
  const getWorktime = () => {
    const weekday = new Date().getDay();
    return (
      restaurant.workdays[weekday].open_time.slice(0, -3) +
      "-" +
      restaurant.workdays[weekday].close_time.slice(0, -3)
    );
  };
  const { name, categories, logo } = restaurant;
  const navigation = useNavigation();
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  return (
    <MapRest
      onPress={() => navigation.replace("Restaurant", { id: restaurant.id })}
    >
      <MapRestLeft>
        <Bold14 color={global.colors.textColor}>{name}</Bold14>
        <View>
          <Book12
            style={{ marginTop: 5, maxWidth: "75%" }}
            color={global.colors.textColor2}
          >
            {restaurant[`description_${current_lang}`].slice(0, 80)}
          </Book12>
        </View>
        <MapRestLeftInfo>
          <Clock />
          <Book12 style={{ marginLeft: 5 }} color={global.colors.textColor}>
            {getWorktime()}
          </Book12>
          {/* <View width={5} /> */}
          <Book12 style={{ marginLeft: 10 }} color={global.colors.textColor}>
            {categories[0][`name_${current_lang}`]}
          </Book12>
        </MapRestLeftInfo>
      </MapRestLeft>
      <Image
        style={{
          width: 100,
          height: 100,
          borderRadius: 100,
          zIndex: 9999,
          position: "absolute",
          right: 0,
        }}
        source={{ url: logo }}
      />
    </MapRest>
  );
}

const MapRest = styled.TouchableOpacity`
  width: ${global.strings.width - 20}px;
  /* margin: 0 10px; */
  align-self: center;
  flex-direction: row;
  justify-content: space-between;
  position: absolute;
  bottom: 30px;
  z-index: 9999;
`;

const MapRestLeft = styled.View`
  padding: 5px 8px;
  width: 100%;
  min-height: 100px;
  background-color: ${global.colors.white};
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  border-top-right-radius: 120px;
  border-bottom-right-radius: 120px;
  overflow: hidden;
  z-index: 9999;
`;

const MapRestLeftInfo = styled.View`
  flex-direction: row;
  margin-top: 15px;
  align-items: center;
`;

const MapRestRight = styled.View`
  /* position: absolute;
  right: -10px; */
`;
