import React from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";

export default function LoadingBlock() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Image
        source={global.images.loaderGif}
        style={{ resizeMode: "contain", maxWidth: "100%" }}
      />
    </View>
  );
}
