import React from "react";
import { Image } from "react-native";
import styled from "styled-components";
import { Bold14, SemiBold14 } from "../../resources/palettes";
import global from "../../resources/global";
import { useNavigation } from "@react-navigation/native";

export default function OrdersBlock({
  name,
  price,
  status,
  statusColor,
  image,
  id,
}) {
  const navigation = useNavigation();
  return (
    <HistoryOrderBlock
      onPress={() => navigation.navigate("OrderScreen", { id })}
    >
      <HistoryOrderLeft>
        <Bold14 color={global.colors.textColor}>{name}</Bold14>
        <SemiBold14 style={{ marginTop: 10 }} color={global.colors.textColor2}>
          {price} UZS
        </SemiBold14>
        <SemiBold14 style={{ marginTop: 10 }} color={statusColor}>
          {status}
        </SemiBold14>
      </HistoryOrderLeft>
      <HistoryOrderRight>
        <Image
          style={{
            width: 100,
            height: 100,
            resizeMode: "contain",
            borderRadius: 100,
            overflow: "hidden",
          }}
          source={{ uri: image }}
        />
      </HistoryOrderRight>
    </HistoryOrderBlock>
  );
}

const HistoryOrderBlock = styled.TouchableOpacity`
  position: relative;
  flex: 1;
  height: 100px;
  flex-direction: row;
  justify-content: space-between;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  margin-top: 10px;
  elevation: 5;
  background-color: white;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  border-top-right-radius: 100px;
  border-bottom-right-radius: 100px;
`;

const HistoryOrderLeft = styled.View`
  padding: 10px;
  background-color: ${global.colors.white};
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
`;

const HistoryOrderRight = styled.View`
  box-shadow: 0px 4px 2px rgba(0, 0, 0, 0.18);
  position: absolute;
  right: 0;
  top: 0;
`;
