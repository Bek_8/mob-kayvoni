import React from "react";
import { Bold14, Book14 } from "../../resources/palettes";
import styled from "styled-components";
import global from "../../resources/global";

export default function StatusBlock({ text, status, bgColor, nameColor }) {
  return (
    <StatusWrap style={{ paddingHorizontal: 15 }} color={bgColor}>
      <Bold14 color={nameColor}>{text}</Bold14>
      <Book14 color={nameColor}>{status}</Book14>
    </StatusWrap>
  );
}

const StatusWrap = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  height: 40px;
  background-color: ${({ color = "#6FCF97" }) => color};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 20px;
`;
