import React from "react";
import { Alert, TouchableOpacity, View } from "react-native";
import Logo from "../../assets/img/logo.svg";
import ArrowBack from "../../assets/img/arrow-back.svg";
import Cart from "../../assets/img/cart.svg";
import RemoveButton from "../../assets/img/removeButton.svg";
import { Bold16, Bold8 } from "../../resources/palettes";
import CustomStatusBar from "./CustomStatusBar";
import { useNavigation } from "@react-navigation/native";
import { HeaderWrapp } from "../../resources/styles";
import global from "../../resources/global";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { cartClear } from "../redux/actions/cartAction";
import { useTranslation } from "react-i18next";

export default ({
  title = "KAYVONI",
  enableBack = false,
  disableCart = false,
  removeButton = false,
}) => {
  const navigation = useNavigation();
  const { cart, restaurant_id } = useSelector((state) => state.cart);
  const { token } = useSelector((state) => state.auth);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleClear = () => {
    Alert.alert(
      t("resetCart"),
      "",
      [
        {
          text: t("cancel"),
        },
        {
          text: t("resum"),
          onPress: () => dispatch(cartClear()),
        },
      ],
      { cancelable: false }
    );
  };
  return (
    <>
      <CustomStatusBar />
      <HeaderWrapp>
        {enableBack ? (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <ArrowBack />
          </TouchableOpacity>
        ) : (
          <Logo />
        )}
        <Bold16>{title.toUpperCase()}</Bold16>
        <View style={{ flexDirection: "row" }}>
          {(disableCart || !token) && !removeButton && (
            <Cart style={{ opacity: 0 }} />
          )}
          {!disableCart && token && (
            <TouchableOpacity onPress={() => navigation.navigate("CartScreen")}>
              <CartBlock>
                <Cart />
                <CartCount>
                  <Bold8>{cart.length}</Bold8>
                </CartCount>
              </CartBlock>
            </TouchableOpacity>
          )}
          {removeButton &&
            (cart.length || (restaurant_id && token) ? (
              <TouchableOpacity onPress={() => handleClear()}>
                <RemoveButton />
              </TouchableOpacity>
            ) : (
              <Cart style={{ opacity: 0 }} />
            ))}
        </View>
      </HeaderWrapp>
    </>
  );
};

const CartBlock = styled.View`
  position: relative;
`;

const CartCount = styled.View`
  position: absolute;
  width: 15px;
  height: 11px;
  background-color: ${global.colors.red};
  top: -7px;
  right: -5px;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
`;
