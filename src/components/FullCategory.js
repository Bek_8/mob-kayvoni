import React from "react";
import { View, Image } from "react-native";
import CatBg from "../../assets/img/catBg.svg";
import { SemiBold18 } from "../../resources/palettes";
import {
  CategoryFull,
  CategoryFullLeft,
  CategoryFullRight,
} from "../../resources/styles";

export default function CategoryBlock({ name, image, color }) {
  return (
    <CategoryFull color={color}>
      <CategoryFullLeft>
        <SemiBold18>{name}</SemiBold18>
      </CategoryFullLeft>
      <CategoryFullRight>
        <CatBg />
        <View style={{ position: "absolute", bottom: 5, right: 0 }}>
          <Image
            style={{ width: 53, height: 53, resizeMode: "contain" }}
            source={{ uri: image }}
          />
        </View>
      </CategoryFullRight>
    </CategoryFull>
  );
}
