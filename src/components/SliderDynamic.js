import React from "react";
import { Image, View } from "react-native";
import global from "../../resources/global";
import Swiper from "react-native-swiper";

export default SliderDynamic = ({ slider = [] }) => {
  return (
    <Swiper
      style={{ height: 180 }}
      showsPagination={false}
      dot={false}
      loop={true}
      autoplay
      autoplayTimeout={2.5}
      autoplayDirection={true}
    >
      {slider.map((inner, key) => (
        <Image
          key={`${inner.id} ${key}`}
          style={{
            width: global.strings.width,
            height: 180,
            resizeMode: "cover",
          }}
          source={{ uri: inner }}
        />
      ))}
    </Swiper>
  );
};
