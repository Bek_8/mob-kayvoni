import React from "react";
import { View, TouchableOpacity } from "react-native";
import global from "../../resources/global";
import { Book12, Bold14 } from "../../resources/palettes";
import { FoodMenuBlock } from "../../resources/styles";

export default function FoodMenu({ name, price, count, sort }) {
  return (
    <FoodMenuBlock>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          flexWrap: "wrap",
          flex: 1,
        }}
      >
        <Book12 color={global.colors.textColor}>x{count}</Book12>
        <View width={6} />
        <Book12 style={{ maxWidth: 300 }} color={global.colors.textColor}>
          {name} {sort}
        </Book12>
      </View>
      <Book12 color={global.colors.textColor}>{price} UZS</Book12>
    </FoodMenuBlock>
  );
}
