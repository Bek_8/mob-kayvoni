import React, { useState } from "react";
import { View } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import HistoryOrdersBlock from "../components/HistoryOrdersBlock";
import { Book16 } from "../../resources/palettes";
import Star from "../../assets/img/bigStar.svg";
import EmptyStar from "../../assets/img/empty-star.svg";
import Stars from "react-native-stars";
import { useSelector } from "react-redux";
import { ActivityIndicator } from "react-native-paper";
import ApiService from "../services/api";
import { useTranslation } from "react-i18next";

export default function HistoryOrderBlock({
  name,
  price,
  status,
  statusColor,
  image,
  rating = 0,
  id,
}) {
  const [newRating, setNewRating] = useState(rating);
  const [loading, setLoading] = useState(false);
  const { token } = useSelector((state) => state.auth);
  const { t } = useTranslation();
  const ratingStatus = () => {
    if (!newRating) return t("rateOrder");
    else return t("rateThank");
  };
  const Loader = () => (
    <View
      style={{
        position: "absolute",
        zIndex: 99,
        top: 10,
        right: 0,
        left: 0,
        bottom: 0,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(255,255,255,.6)",
        borderRadius: 20,
      }}
    >
      <ActivityIndicator size={"large"} color={global.colors.main} />
    </View>
  );
  const leaveRating = (value) => {
    setLoading(true);
    const data = {
      rate: value,
      feedback: " ",
    };
    ApiService.postEvent(`/order/${id}/rating`, token, data).then((value) => {
      if (value.statusCode !== 200) {
        setNewRating(0);
      }
      setLoading(false);
    });
  };
  return (
    <View>
      {loading && <Loader />}
      <HistoryOrdersBlock
        name={name}
        price={price}
        status={status}
        statusColor={statusColor}
        image={image}
        id={id}
      />
      <ReviewBlock>
        <View style={{ paddingHorizontal: 10 }}>
          <Book16 color={global.colors.textColor2}>{ratingStatus()}</Book16>
        </View>
        <View style={{ flex: 1 }}>
          <StarsWrap
            style={{ paddingHorizontal: 10, justifyContent: "center" }}
          >
            <Stars
              update={(val) => {
                setNewRating(val);
                leaveRating(val);
              }}
              disabled={newRating > 0}
              half={false}
              default={rating}
              count={5}
              spacing={20}
              fullStar={<Star />}
              emptyStar={<EmptyStar />}
            />
          </StarsWrap>
        </View>
      </ReviewBlock>
    </View>
  );
}

const StarsWrap = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 30px;
  background-color: ${global.colors.white};
  border-radius: 10px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
`;

const ReviewBlock = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
