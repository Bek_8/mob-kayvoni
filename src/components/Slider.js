import React from "react";
import { Image } from "react-native";
import global from "../../resources/global";
import Swiper from "react-native-swiper";
import { useSelector } from "react-redux";

export default Slider = () => {
  const { slider, sliderLoading, error } = useSelector((state) => state.slider);
  if (sliderLoading) return null;
  return (
    <Swiper
      height={180}
      showsPagination={false}
      dot={false}
      loop={true}
      autoplay
      autoplayTimeout={2.5}
      autoplayDirection={true}
    >
      {slider.map((slider, key) => (
        <Image
          key={`${slider.id} ${key}`}
          style={{
            width: global.strings.width,
            height: 180,
            resizeMode: "cover",
          }}
          source={{ uri: slider.image }}
        />
      ))}
    </Swiper>
  );
};
