import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "./reducers/rootReducer";
import { persistStore, persistReducer } from "redux-persist";
import asyncStorage from "@react-native-community/async-storage";
import thunk from "redux-thunk";

const persistConfig = {
  key: "system",
  storage: asyncStorage,
  whitelist: ["auth", "cart", "language"], // which reducer want to store
};
const persistReduce = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistReduce, applyMiddleware(thunk));
export const persistor = persistStore(store);
