import * as searchTypes from "../actionTypes/searchActionTypes";

const initialState = {
  search: { food: [], restaurant: [] },
  searchLoading: false,
  searchError: null,
  keyword: "",
  from: "",
  to: "",
  category_ids: [],
  type_ids: [],
  sort_ids: [],
};
export function searchReducer(state = initialState, { type, payload }) {
  switch (type) {
    case searchTypes.STARTING_SEARCH:
      return { ...state, searchLoading: true };
    case searchTypes.SUCCESS_SEARCH:
      return { ...state, searchLoading: false, search: payload };
    case searchTypes.CHANGE_DATA_SEARCH: {
      const { key, value } = payload;
      return { ...state, [key]: value };
    }
    case searchTypes.CLEAR_FILTER_SEARCH:
      return {
        ...initialState,
        search: state.search,
        searchLoading: false,
        searchError: false,
      };
    case searchTypes.FAIL_SEARCH:
      return {
        ...state,
        searchLoading: false,
        searchError: payload,
        search: null,
      };
    default:
      return state;
  }
}
