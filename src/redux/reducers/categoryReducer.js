import * as categoryTypes from "../actionTypes/categoryActionTypes";

const initialState = {
  category: [],
  categoryScreen: [],
  categoryScreenLoading: false,
  categoryScreenError: null,
};
export function categoryReducer(state = initialState, { type, payload }) {
  switch (type) {
    case categoryTypes.STARTING_CATEGORY:
      return { ...state, categoryScreenLoading: true };
    case categoryTypes.SUCCESS_CATEGORY:
      return { ...state, categoryScreenLoading: false, category: payload };
    case categoryTypes.FAIL_CATEGORY:
      return {
        ...state,
        categoryScreenLoading: false,
        categoryScreenError: payload,
        category: null,
      };
    case categoryTypes.STARTING_CATEGORY_BY_ID:
      return { ...state, categoryScreenLoading: true };
    case categoryTypes.SUCCESS_CATEGORY_BY_ID:
      return {
        ...state,
        categoryScreenLoading: false,
        categoryScreen: payload,
      };
    case categoryTypes.FAIL_CATEGORY_BY_ID:
      return {
        ...state,
        categoryScreenLoading: false,
        categoryScreenError: payload,
        categoryScreen: null,
      };
    default:
      return state;
  }
}
