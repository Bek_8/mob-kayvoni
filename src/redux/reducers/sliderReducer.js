import * as sliderTypes from "../actionTypes/sliderActionTypes";

const initialState = {
  slider: [],
  sliderLoading: false,
  error: null,
};
export function sliderReducer(state = initialState, { type, payload }) {
  switch (type) {
    case sliderTypes.STARTING_SLIDER:
      return { ...state, sliderLoading: true };
    case sliderTypes.SUCCESS_SLIDER:
      return { ...state, sliderLoading: false, slider: payload };
    case sliderTypes.FAIL_SLIDER:
      return { ...state, sliderLoading: false, error: payload, slider: null };
    default:
      return state;
  }
}
