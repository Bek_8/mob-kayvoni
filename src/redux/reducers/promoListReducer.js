import * as promoListTypes from "../actionTypes/promoListActionTypes";

const initialState = {
  promoList: {},
  promoListLoading: false,
  promoListError: null,
};
export function promoListReducer(state = initialState, { type, payload }) {
  switch (type) {
    case promoListTypes.STARTING_PROMO_LIST:
      return { ...state, promoListLoading: true };
    case promoListTypes.SUCCESS_PROMO_LIST:
      return { ...state, promoListLoading: false, promoList: payload };
    case promoListTypes.FAIL_PROMO_LIST:
      return {
        ...state,
        promoListLoading: false,
        promoListError: payload,
        promoList: null,
      };
    default:
      return state;
  }
}
