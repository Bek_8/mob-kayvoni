import {
  FETCH_RESTAURANT_BEGIN,
  FETCH_RESTAURANT_FAILURE,
  FETCH_RESTAURANT_SUCCESS,
} from "../actionTypes/restaurantTypes";

const initialState = {
  restaurants: [],
  loading: false,
  error: null,
};
export function restaurantReducer(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_RESTAURANT_BEGIN:
      return { ...state, loading: true };
    case FETCH_RESTAURANT_SUCCESS:
      return { ...state, loading: false, restaurants: payload, error: null };
    case FETCH_RESTAURANT_FAILURE:
      return { ...state, error: payload };
    default:
      return state;
  }
}
