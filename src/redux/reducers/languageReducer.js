const initialState = {
  language: "ru",
};
export function languageReducer(state = initialState, { type, payload }) {
  switch (type) {
    case "SWITCH_LANGUAGE":
      return { ...state, language: payload };
    default:
      return state;
  }
}
