import * as userOrdersTypes from "../actionTypes/userOrdersActionType";

const initialState = {
  userOrders: [],
  userOrdersLoading: false,
  userOrdersError: null,
  userOrder: null,
};
export function userOrdersReducer(state = initialState, { type, payload }) {
  switch (type) {
    case userOrdersTypes.STARTING_USER_ORDERS:
      return { ...state, userOrdersLoading: true };
    case userOrdersTypes.SUCCESS_USER_ORDERS:
      return { ...state, userOrders: payload, userOrdersLoading: false };
    case userOrdersTypes.FAIL_USER_ORDERS:
      return {
        ...state,
        userOrdersLoading: false,
        userOrdersError: payload,
        userOrders: [],
      };
    case userOrdersTypes.STARTING_USER_ORDER_ID:
      return { ...state, userOrdersLoading: true };
    case userOrdersTypes.SUCCESS_USER_ORDER_ID:
      return { ...state, userOrder: payload, userOrdersLoading: false };
    case userOrdersTypes.FAIL_USER_ORDER_ID:
      return {
        ...state,
        userOrdersLoading: false,
        userOrdersError: payload,
        userOrder: null,
      };
    default:
      return state;
  }
}
