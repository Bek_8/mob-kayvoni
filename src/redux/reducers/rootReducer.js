import { cartReducer } from "./cartReducer";
import { combineReducers } from "redux";
import { restaurantReducer } from "./restaurantReducer";
import { authReducer } from "./authReducer";
import { sliderReducer } from "./sliderReducer";
import { mainReducer } from "./mainReducer";
import { categoryReducer } from "./categoryReducer";
import { restaurantsReducer } from "./restaurantsReducer";
import { foodReducer } from "./foodReducer";
import { promoReducer } from "./promoReducer";
import { promoListReducer } from "./promoListReducer";
import { searchReducer } from "./searchReducer";
import { typeReducer } from "./typeReducer";
import { userReducer } from "./userReducer";
import { userOrdersReducer } from "./userOrdersReducer";
import { languageReducer } from "./languageReducer";
export const rootReducer = combineReducers({
  cart: cartReducer,
  main: mainReducer,
  category: categoryReducer,
  restaurantReducer,
  auth: authReducer,
  slider: sliderReducer,
  restaurant: restaurantsReducer,
  food: foodReducer,
  promo: promoReducer,
  promoList: promoListReducer,
  search: searchReducer,
  type: typeReducer,
  user: userReducer,
  userOrders: userOrdersReducer,
  language: languageReducer,
});
