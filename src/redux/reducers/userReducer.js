import * as userTypes from "../actionTypes/userActionType";

const initialState = {
  user: {},
  userLoading: false,
  userError: null,
};
export function userReducer(state = initialState, { type, payload }) {
  switch (type) {
    case userTypes.STARTING_USER:
      return { ...state, userLoading: true };
    case userTypes.SUCCESS_USER:
      return { ...state, userLoading: false, user: payload };
    case userTypes.FAIL_USER:
      return { ...state, userLoading: false, userError: payload, user: null };
    default:
      return state;
  }
}
