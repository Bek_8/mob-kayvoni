import * as authTypes from "../actionTypes/authActionTypes";

const initialState = {
  token: null,
  notification_token: null,
  name: "",
  phone: null,
  loading: false,
};
export function authReducer(state = initialState, { type, payload }) {
  switch (type) {
    case authTypes.STARTING_AUTHORIZE:
    case authTypes.STARTING_LOGOUT:
    case authTypes.STARTING_VERIFY: {
      return { ...state, loading: true };
    }
    case authTypes.SUCCES_VERIFY:
      return { ...state, loading: false, phone: payload };
    case authTypes.FAIL_VERIFY:
      return {
        ...state,
        loading: false,
        phone: null,
        token: null,
      };
    case authTypes.HANDLE_CHANGE:
      const { key, value } = payload;
      return { ...state, [key]: value };

    case authTypes.FAIL_LOGOUT:
    case authTypes.SUCCES_REGISTER:
      return { ...state, loading: false };
    case authTypes.SUCCES_AUTHORIZE:
      return {
        ...state,
        loading: false,
        token: payload.token,
      };
    case authTypes.FAIL_AUTHORIZE:
      return { ...state, loading: false, token: null };
    case authTypes.SUCCESS_LOGOUT:
      return { ...initialState };

    default:
      return state;
  }
}
