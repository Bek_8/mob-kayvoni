import * as restaurantTypes from "../actionTypes/restaurantActionTypes";

const initialState = {
  restaurant: null,
  restaurantLoading: false,
  restaurantError: null,
};
export function restaurantsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case restaurantTypes.STARTING_RESTAURANT:
      return { ...state, restaurantLoading: true };
    case restaurantTypes.SUCCESS_RESTAURANT:
      return { ...state, restaurant: payload, restaurantLoading: false };
    case restaurantTypes.FAIL_RESTAURANT:
      return {
        ...state,
        restaurantError: payload,
        restaurant: null,
        restaurantLoading: false,
      };
    default:
      return state;
  }
}
