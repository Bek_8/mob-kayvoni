import * as mainTypes from "../actionTypes/mainActionTypes";

const initialState = {
  main: {},
  homeScreenLoading: false,
  homeScreenError: null,
};
export function mainReducer(state = initialState, { type, payload }) {
  switch (type) {
    case mainTypes.STARTING_HOME_SCREEN:
      return { ...state, homeScreenLoading: true };
    case mainTypes.SUCCESS_HOME_SCREEN:
      return { ...state, homeScreenLoading: false, main: payload };
    case mainTypes.FAIL_HOME_SCREEN:
      return {
        ...state,
        homeScreenLoading: false,
        homeScreenError: payload,
        main: null,
      };
    default:
      return state;
  }
}
