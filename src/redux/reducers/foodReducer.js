import * as foodTypes from "../actionTypes/foodActionTypes";

const initialState = {
  food: null,
  foodLoading: false,
  foodError: null,
};
export function foodReducer(state = initialState, { type, payload }) {
  switch (type) {
    case foodTypes.STARTING_FOOD:
      return { ...state, foodLoading: true };
    case foodTypes.SUCCESS_FOOD:
      return { ...state, foodLoading: false, food: payload };
    case foodTypes.FAIL_FOOD:
      return { ...state, foodLoading: false, foodError: payload, food: null };
    default:
      return state;
  }
}
