import * as cartTypes from "../actionTypes/cartActionTypes";

const initialState = {
  cart: [],
  restaurant: null,
  cartLoading: false,
  cartError: null,
  restaurant_id: null,
  seats_count: 1,
  reserved_time: null,
  comment: "",
};
export function cartReducer(state = initialState, { type, payload }) {
  switch (type) {
    case cartTypes.STARTING_CART:
      return { ...state, cartLoading: true, cartError: null };
    case cartTypes.STOP_CART:
      return { ...state, cartLoading: false, cartError: null };
    case cartTypes.SUCCESS_CART:
      return {
        ...state,
        restaurant: payload,
        cartLoading: false,
        cartError: null,
        reserved_time: null,
      };
    case cartTypes.FAIL_CART:
      return {
        ...state,
        cartLoading: false,
        cartError: payload,
        restaurant: null,
        reserved_time: null,
      };
    case cartTypes.CHANGE_DATA_CART:
      const { key, value } = payload;
      return { ...state, [key]: value };
    case cartTypes.SUCCESS_CHECKOUT:
      return {
        ...state,
        url: payload,
        cartLoading: false,
        comment: "",
      };
    case cartTypes.CART_ADD: {
      const { food, restaurant_id } = payload;
      return {
        ...state,
        cart: [food, ...state.cart],
        restaurant_id,
        cartError: null,
        cartLoading: false,
      };
    }
    case cartTypes.CART_SWITCH: {
      return {
        ...state,
        cart: [],
        restaurant_id: payload,
        cartError: null,
        cartLoading: false,
      };
    }
    case cartTypes.CART_REMOVE: {
      return {
        ...state,
        cart: state.cart.filter((item, index) => index != payload),
        cartError: null,
      };
    }
    case cartTypes.CART_CLEAR:
      return {
        ...initialState,
      };
    case cartTypes.CART_UPDATE: {
      const { key, count } = payload;
      const update_data = state.cart;
      update_data[key].count = count;
      return {
        ...state,
        cart: update_data,
        cartError: null,
      };
    }
    default:
      return state;
  }
}
