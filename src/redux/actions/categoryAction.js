import ApiService from "../../services/api";
import * as categoryTypes from "../actionTypes/categoryActionTypes";

export const getCategory = () => (dispatch) => {
  dispatch(categoryBegin());
  return ApiService.getResources("/category", null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(categorySuccess(value.data));
      }
    })
    .catch((error) => dispatch(categoryFail(error)));
};

export function categoryBegin() {
  return {
    type: categoryTypes.STARTING_CATEGORY,
  };
}

export function categorySuccess(data) {
  return {
    type: categoryTypes.SUCCESS_CATEGORY,
    payload: data,
  };
}

export function categoryFail(error) {
  return {
    type: categoryTypes.FAIL_CATEGORY,
    payload: error,
  };
}

export const getCategoryById = (id) => (dispatch) => {
  dispatch(categoryByIdBegin());
  return ApiService.getResources(`/category/${id}/restaurants`, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(categoryByIdSuccess(value.data));
      }
    })
    .catch((error) => dispatch(categoryByIdFail(error)));
};

export function categoryByIdBegin() {
  return {
    type: categoryTypes.STARTING_CATEGORY_BY_ID,
  };
}

export function categoryByIdSuccess(data) {
  return {
    type: categoryTypes.SUCCESS_CATEGORY_BY_ID,
    payload: data,
  };
}

export function categoryByIdFail(error) {
  return {
    type: categoryTypes.FAIL_CATEGORY_BY_ID,
    payload: error,
  };
}
