import ApiService from "../../services/api";
import * as typeTypes from "../actionTypes/typeActionTypes";

export const getType = (type) => (dispatch) => {
  dispatch(typeBegin());
  return ApiService.getResources("/" + type, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(typeSuccess(value.data));
      }
    })
    .catch((error) => dispatch(typeFail(error)));
};

export function typeBegin() {
  return {
    type: typeTypes.STARTING_TYPES,
  };
}

export function typeSuccess(data) {
  return {
    type: typeTypes.SUCCESS_TYPES,
    payload: data,
  };
}

export function typeFail(error) {
  return {
    type: typeTypes.FAIL_TYPES,
    payload: error,
  };
}
