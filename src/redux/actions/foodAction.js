import ApiService from "../../services/api";
import * as foodTypes from "../actionTypes/foodActionTypes";

export const getFood = (id) => (dispatch) => {
  dispatch(foodBegin());
  return ApiService.getResources(`/show/food/${id}`, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(foodSuccess(value.data));
      }
    })
    .catch((error) => dispatch(foodFail(error)));
};

export function foodBegin() {
  return {
    type: foodTypes.STARTING_FOOD,
  };
}

export function foodSuccess(data) {
  return {
    type: foodTypes.SUCCESS_FOOD,
    payload: data,
  };
}

export function foodFail(error) {
  return {
    type: foodTypes.FAIL_FOOD,
    payload: error,
  };
}
