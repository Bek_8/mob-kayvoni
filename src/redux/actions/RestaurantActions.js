import ApiService from "../../services/api";
import {
  FETCH_RESTAURANT_BEGIN,
  FETCH_RESTAURANT_FAILURE,
  FETCH_RESTAURANT_SUCCESS,
} from "../actionTypes/RestaurantTypes";

export const getRestaurant = (dispatch) => {
  dispatch(fetchProductsBegin());
  return ApiService.getResources()
    .then(handleErrors)
    .then((value) => {
      dispatch(fetchProductsSuccess(value.data));
    })
    .catch((error) => dispatch(fetchProductsFailure(error)));
};

export const fetchProductsBegin = () => ({
  type: FETCH_RESTAURANT_BEGIN,
});

export const fetchProductsSuccess = (restaurant) => ({
  type: FETCH_RESTAURANT_SUCCESS,
  payload: restaurant,
});

export const fetchProductsFailure = (error) => ({
  type: FETCH_RESTAURANT_FAILURE,
  payload: error,
});

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
