import ApiService from "../../services/api";
import * as userOrdersTypes from "../actionTypes/userOrdersActionType";

export const getUserOrders = (token) => (dispatch) => {
  dispatch(userOrdersBegin());
  return ApiService.getResources("/user/orders", token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(userOrdersSuccess(value.data));
      }
    })
    .catch((error) => dispatch(userOrdersFail(error)));
};
export const getUserOrderById = (token, id) => (dispatch) => {
  dispatch(userOrdersBegin());
  return ApiService.getResources("/user/order/" + id, token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(userOrderByIdSuccess(value.data));
      }
    })
    .catch((error) => dispatch(userOrderByIdFail(error)));
};

export function userOrdersBegin() {
  return {
    type: userOrdersTypes.STARTING_USER_ORDERS,
  };
}

export function userOrdersSuccess(data) {
  return {
    type: userOrdersTypes.SUCCESS_USER_ORDERS,
    payload: data,
  };
}

export function userOrdersFail(error) {
  return {
    type: userOrdersTypes.FAIL_USER_ORDERS,
    payload: error,
  };
}

export function userOrderByIdSuccess(data) {
  return {
    type: userOrdersTypes.SUCCESS_USER_ORDER_ID,
    payload: data,
  };
}

export function userOrderByIdFail(error) {
  return {
    type: userOrdersTypes.FAIL_USER_ORDER_ID,
    payload: error,
  };
}
