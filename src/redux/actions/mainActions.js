import ApiService from "../../services/api";
import * as mainTypes from "../actionTypes/mainActionTypes";

export const fetchMainScreen = () => (dispatch) => {
  dispatch(mainBegin());
  return ApiService.getResources("/main", null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(mainSuccess(value.data));
      }
    })
    .catch((error) => dispatch(mainFail(error)));
};

export function mainBegin() {
  return {
    type: mainTypes.STARTING_HOME_SCREEN,
  };
}

export function mainSuccess(data) {
  return {
    type: mainTypes.SUCCESS_HOME_SCREEN,
    payload: data,
  };
}

export function mainFail(error) {
  return {
    type: mainTypes.FAIL_HOME_SCREEN,
    payload: error,
  };
}
