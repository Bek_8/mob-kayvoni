import ApiService from "../../services/api";
import * as promoTypes from "../actionTypes/promoActionTypes";

export const getPromoList = (id) => (dispatch) => {
  dispatch(promoBegin());
  return ApiService.getResources(`/show/promo/restaurant/${id}`, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(promoSuccess(value.data));
      }
    })
    .catch((error) => dispatch(promoFail(error)));
};

export function promoBegin() {
  return {
    type: promoTypes.STARTING_PROMO,
  };
}

export function promoSuccess(data) {
  return {
    type: promoTypes.SUCCESS_PROMO,
    payload: data,
  };
}

export function promoFail(error) {
  return {
    type: promoTypes.FAIL_PROMO,
    payload: error,
  };
}
