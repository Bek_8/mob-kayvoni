import { Alert } from "react-native";
import ApiService from "../../services/api";
import * as authTypes from "../actionTypes/authActionTypes";

export const verificationUser = (phone, callback) => (dispatch) => {
  dispatch(verificationBegin());
  const body = {
    phone,
  };
  return ApiService.postEvent("/verification", null, body)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(verificationSuccess(phone));
      }
    })
    .then(() => {
      if (callback) callback();
    })
    .catch((error) => dispatch(verificationFailure(error)));
};
export const verificationBegin = () => ({
  type: authTypes.STARTING_VERIFY,
});

export const handleChangeAuth = (payload) => ({
  type: authTypes.HANDLE_CHANGE,
  payload,
});
export const verificationSuccess = (phone) => ({
  type: authTypes.SUCCES_VERIFY,
  payload: phone,
});

export const verificationFailure = (error) => ({
  type: authTypes.FAIL_VERIFY,
  payload: error,
});

export const authorizeUser = (phone, verify_code, callback) => (dispatch) => {
  const body = {
    phone,
    verify_code,
  };
  dispatch(authorizeBegin());
  return ApiService.postEvent("/auth", null, body)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(authorizeSuccess(value.data));
        if (callback) callback();
      } else {
        throw value;
      }
    })
    .catch((error) => {
      dispatch(authorizeFailure(error));
    });
};

export const authorizeBegin = () => ({
  type: authTypes.STARTING_AUTHORIZE,
});

export const authorizeSuccess = (data) => ({
  type: authTypes.SUCCES_AUTHORIZE,
  payload: data,
});

export const authorizeFailure = (error) => {
  Alert.alert(error.message);
  return { type: authTypes.FAIL_AUTHORIZE, payload: error };
};

export const updateUser = (
  { name = null, notification_token = null },
  token,
  callback
) => (dispatch) => {
  const body = {
    ...(!!name && { name }),
    ...(!!notification_token && { notification_token }),
  };
  dispatch(registerBegin());
  return ApiService.updateEvent("/set/token", token, body)
    .then(() => {
      if (name) {
        dispatch(handleChangeAuth({ key: "name", value: name }));
      }
      dispatch(registerSuccess());
      if (callback) callback();
    })
    .catch(() => dispatch(registerSuccess()));
};

export const getLogout = (token, callback) => (dispatch) => {
  dispatch(logoutBegin());
  return ApiService.getResources("/logout", token)
    .then((value) => {
      dispatch(logoutSuccess());
      callback();
    })
    .catch(() => {
      dispatch(logoutSuccess());
      callback();
    });
};
export function logoutBegin() {
  return {
    type: authTypes.STARTING_LOGOUT,
  };
}

export function logoutSuccess(data) {
  return {
    type: authTypes.SUCCESS_LOGOUT,
    payload: data,
  };
}

export function logoutFail() {
  return {
    type: authTypes.FAIL_LOGOUT,
  };
}
export const registerBegin = () => ({
  type: authTypes.STARTING_REGISTER,
});

export const registerSuccess = () => ({
  type: authTypes.SUCCES_REGISTER,
});

export const registerFailure = (error) => ({
  type: authTypes.FAIL_REGISTER,
  payload: error,
});
