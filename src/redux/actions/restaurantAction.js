import ApiService from "../../services/api";
import * as restaurantTypes from "../actionTypes/restaurantActionTypes";

export const getRestaurant = (id) => (dispatch) => {
  dispatch(restaurantBegin());
  return ApiService.getResources(`/show/restaurant/${id}`, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(restaurantSuccess(value.data));
      }
    })
    .catch((error) => dispatch(restaurantFail(error)));
};

export function restaurantBegin() {
  return {
    type: restaurantTypes.STARTING_RESTAURANT,
  };
}

export function restaurantSuccess(data) {
  return {
    type: restaurantTypes.SUCCESS_RESTAURANT,
    payload: data,
  };
}

export function restaurantFail(error) {
  return {
    type: restaurantTypes.FAIL_RESTAURANT,
    payload: error,
  };
}
