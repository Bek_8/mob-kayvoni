export const availableLanguages = ["ru", "en", "uz"];

export function switchLanguage(payload) {
  return {
    type: "SWITCH_LANGUAGE",
    payload,
  };
}
