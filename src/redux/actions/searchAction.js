import ApiService from "../../services/api";
import * as searchTypes from "../actionTypes/searchActionTypes";
import { store } from "../rootConfig";
export const getSearchList = (callback) => (dispatch) => {
  const {
    from,
    to,
    keyword,
    category_ids,
    type_ids,
    sort_ids,
  } = store.getState().search;
  const price_range = [!!from.length ? from : 0, !!to.length ? to : 9999999999];
  const body = {
    price_range: `${price_range[0]},${price_range[1]}`,
    ...(keyword.length > 0 && {
      keyword: keyword,
    }),
    ...(category_ids.length > 0 && {
      category_ids: JSON.stringify(category_ids.map((item) => item.id)),
    }),
    ...(type_ids.length > 0 && {
      type_ids: JSON.stringify(type_ids.map((item) => item.id)),
    }),
    ...(sort_ids.length > 0 && {
      sort_ids: JSON.stringify(sort_ids.map((item) => item.id)),
    }),
  };
  dispatch(searchBegin());
  return ApiService.getResources("/search?" + new URLSearchParams(body), null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(searchSuccess(value.data));
      }
    })
    .then(() => {
      if (callback) callback();
    })
    .catch((error) => dispatch(searchFail(error)));
};

export function searchBegin() {
  return {
    type: searchTypes.STARTING_SEARCH,
  };
}
export function clearFilter() {
  return {
    type: searchTypes.CLEAR_FILTER_SEARCH,
  };
}

export function changeData({ key, value }) {
  return {
    type: searchTypes.CHANGE_DATA_SEARCH,
    payload: { key, value },
  };
}

export function searchSuccess(data) {
  return {
    type: searchTypes.SUCCESS_SEARCH,
    payload: data,
  };
}

export function searchFail(error) {
  return {
    type: searchTypes.FAIL_SEARCH,
    payload: error,
  };
}
