export const STARTING_CART = "STARTING_CART";
export const STOP_CART = "STOP_CART";
export const SUCCESS_CART = "SUCCESS_CART";
export const SUCCESS_CHECKOUT = "SUCCESS_CHECKOUT";
export const FAIL_CART = "FAIL_CART";
export const CHANGE_DATA_CART = "CHANGE_DATA_CART";

export const CART_REMOVE = "CART_REMOVE";
export const CART_SWITCH = "CART_SWITCH";
export const CART_UPDATE = "CART_UPDATE";
export const CART_CLEAR = "CART_CLEAR";
export const CART_ADD = "CART_ADD";
