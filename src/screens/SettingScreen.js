import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import global from "../../resources/global";
import { SemiBold14, SemiBold13, Book14 } from "../../resources/palettes";
import LoadingBlock from "../components/LoadingBlock";
import { getLogout } from "../redux/actions/authAction";
import { switchLanguage } from "../redux/actions/languageAction";

export default function SettingScreen({ navigation }) {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);
  const { language } = useSelector((state) => state.language);
  const changeLang = (lng) => {
    if (lng != language) {
      dispatch(switchLanguage(lng));
      i18n.changeLanguage(lng);
    }
  };
  const handleLogout = () => {
    dispatch(getLogout(token, () => navigation.goBack()));
  };
  return (
    <SettingWrap>
      <SemiBold14 color={global.colors.main}>{t("lang")}</SemiBold14>
      <LangWrap>
        <LangButtons
          style={{
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
            borderRightWidth: 1,
            borderRightColor: "#efefef",
          }}
          onPress={() => changeLang("uz")}
          color={language != "uz" ? global.colors.white : global.colors.main}
        >
          <Book14
            color={language != "uz" ? global.colors.main : global.colors.white}
          >
            UZ
          </Book14>
        </LangButtons>
        <LangButtons
          color={language != "ru" ? global.colors.white : global.colors.main}
          onPress={() => changeLang("ru")}
        >
          <Book14
            color={language != "ru" ? global.colors.main : global.colors.white}
          >
            РУ
          </Book14>
        </LangButtons>
        <LangButtons
          style={{
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
            borderLeftWidth: 1,
            borderLeftColor: "#efefef",
          }}
          color={language != "en" ? global.colors.white : global.colors.main}
          onPress={() => changeLang("en")}
        >
          <Book14
            color={language != "en" ? global.colors.main : global.colors.white}
          >
            EN
          </Book14>
        </LangButtons>
      </LangWrap>
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-end",
          paddingBottom: 20,
        }}
      >
        <LogOutBtn onPress={() => handleLogout()}>
          <SemiBold13>{t("logout")}</SemiBold13>
        </LogOutBtn>
      </View>
    </SettingWrap>
  );
}

const SettingWrap = styled.View`
  padding: 10px;
  flex: 1;
  background-color: ${global.colors.white};
`;

const LangWrap = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  height: 40px;
  background-color: ${global.colors.white};
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
`;

const LangButtons = styled.TouchableOpacity`
  width: 33%;
  justify-content: center;
  align-items: center;
  background-color: ${({ color = global.colors.blue }) => color};
  elevation: 5;
`;

const LangSubmitBtn = styled.TouchableOpacity`
  margin-top: 10px;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 40px;
  background-color: ${global.colors.lightGreen};
  border-radius: 10px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
  elevation: 5;
`;

const LogOutBtn = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  width: 100%;
  align-self: center;
  height: 40px;
  background-color: ${global.colors.red};
  border-radius: 10px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
  elevation: 5;
  /* bottom: 30px; */
  /* left: 10px; */
`;
