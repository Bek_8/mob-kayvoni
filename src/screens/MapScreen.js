import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import MapView, { Marker } from "react-native-maps";
import global from "../../resources/global";
import styled from "styled-components";
import { Bold12 } from "../../resources/palettes";
import MarkerPin from "../../assets/img/marker.svg";
import Close from "../../assets/img/close.svg";
import BigMarkerPin from "../../assets/img/big-pin.svg";
import MapRestBlock from "../components/MapRestBlock";
import { connect } from "react-redux";
class MapScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chosen: 0,
      markers: [],
    };
  }
  findRestaurant() {
    return this.props.search.restaurants.find(
      (item) => item.id === this.state.chosen
    );
  }
  render() {
    const { search } = this.props;
    const { restaurants } = search;
    const { chosen } = this.state;
    return (
      <MapWrapp>
        <MapView
          style={{ width: global.strings.width, height: global.strings.height }}
          initialRegion={{
            latitude: 41.311081,
            longitude: 69.249122,
            latitudeDelta: 0.301,
            longitudeDelta: 0.301,
          }}
        >
          {restaurants.map((item, key) => (
            <Marker
              key={`${key} ${item}`}
              coordinate={{
                latitude: parseFloat(item.lat),
                longitude: parseFloat(item.long),
              }}
              onPress={() => this.setState({ chosen: item.id })}
            >
              {item.id !== chosen ? (
                <Relative>
                  <MarkerPin />
                  <MarkerItem>
                    <Image
                      style={{
                        width: 33,
                        height: 33,
                        borderRadius: 33,
                        overflow: "hidden",
                      }}
                      source={{ uri: item.logo }}
                    />
                  </MarkerItem>
                </Relative>
              ) : (
                <Relative>
                  <BigMarkerPin />
                  <MarkerItem>
                    <Image
                      style={{ width: 145, height: 80, resizeMode: "cover" }}
                      source={{ uri: item.banner[0] }}
                    />
                    <Bold12 style={{ textAlign: "center" }}>{item.name}</Bold12>
                  </MarkerItem>
                </Relative>
              )}
            </Marker>
          ))}
        </MapView>
        {chosen > 0 && (
          <>
            <View style={{ position: "absolute", bottom: 140, right: 20 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: "#fff",
                  borderRadius: 10,
                  width: 40,
                  height: 40,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onPress={() => this.setState({ chosen: false })}
              >
                <Close />
              </TouchableOpacity>
            </View>
            <MapRestBlock restaurant={this.findRestaurant()} />
          </>
        )}
      </MapWrapp>
    );
  }
}
const mapStateToProps = (state) => ({ search: state.search.search });

export default connect(mapStateToProps)(MapScreen);
const MapWrapp = styled.View`
  flex: 1;
  background-color: ${global.colors.white};
  align-items: center;
  justify-content: center;
  position: relative;
`;
const Relative = styled.View`
  position: relative;
  align-items: center;
  justify-content: center;
`;

const MarkerItem = styled.View`
  position: absolute;
  top: 5px;
  left: 4px;
`;
