import React, { useState } from "react";
import {
  TextInput,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import global from "../../resources/global";
import Person from "../../assets/img/person.svg";
import { Book13, Book14, SemiBold13 } from "../../resources/palettes";
import {
  ProfileWrap,
  IconWrapp,
  InputWrapp,
  TextWrapp,
  SubmitButton,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "../redux/actions/authAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function RegisterScreen() {
  const { t } = useTranslation();
  const [name, setName] = useState("");
  const dispatch = useDispatch();
  const { loading, error, token } = useSelector((state) => state.auth);
  const getProfile = () => {
    if (name.length > 4) {
      dispatch(updateUser({ name }, token));
    }
  };

  if (loading) return <LoadingBlock />;
  return (
    <KeyboardAvoidingView
      contentContainerStyle={{
        backgroundColor: "#999",
        flex: 1,
      }}
      behavior={Platform.OS == "android" ? "height" : "padding"}
    >
      <ProfileWrap>
        <ImageBackground
          style={{
            width: "100%",
            height: "100%",
            reseizeMode: "cover",
            justifyContent: "center",
          }}
          source={global.images.profileBg}
        >
          <IconWrapp>
            <Person />
          </IconWrapp>
          <InputWrapp>
            <Book13 style={{ textAlign: "center" }}>{t("enterName")}</Book13>
            <TextInput
              keyboardType={`default`}
              returnKeyType={"done"}
              value={name}
              color={global.colors.main}
              onChangeText={(text) => setName(text)}
              style={{
                width: "100%",
                height: 40,
                backgroundColor: global.colors.white,
                marginTop: 5,
                borderRadius: 10,
                padding: 10,
                textAlign: "center",
              }}
            />
          </InputWrapp>
          {error && <Book14>{error.message}</Book14>}
          <TextWrapp style={{ opacity: 0 }}>
            <Book14>Войдя в аккаунт вы соглашаетесь с</Book14>
            <Book14>условиями использования приложения</Book14>
          </TextWrapp>
          <SubmitButton onPress={() => getProfile()}>
            {/* <SubmitButton onPress={() => navigation.navigate("Profile")}> */}
            <SemiBold13>{t("register")}</SemiBold13>
          </SubmitButton>
        </ImageBackground>
      </ProfileWrap>
    </KeyboardAvoidingView>
  );
}
