import React from "react";
import { View } from "react-native";
import global from "../../resources/global";
import { SemiBold18 } from "../../resources/palettes";
import Paid from "../../assets/img/paid.svg";

export default function PaidScreen() {
  return (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        backgroundColor: global.colors.white,
      }}
    >
      <Paid />
      <SemiBold18 style={{ marginTop: 30 }} color={global.colors.textColor}>
        Оплата проведена успешно
      </SemiBold18>
    </View>
  );
}
