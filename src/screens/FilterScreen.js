import React, { useState } from "react";
import { ScrollView, View, TextInput } from "react-native";
import global from "../../resources/global";
import { SemiBold14, SemiBold13 } from "../../resources/palettes";
import FilterCatButton from "../components/FilterCatButton";
import {
  FilterWrapper,
  FilterInputWrap,
  FilterButtonsWrap,
  FilterButton,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import {
  getSearchList,
  changeData,
  clearFilter,
} from "../redux/actions/searchAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function FilterScreen({ navigation }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    to,
    from,
    category_ids,
    sort_ids,
    type_ids,
    searchLoading,
  } = useSelector((state) => state.search);
  const makeSearch = () => {
    dispatch(getSearchList(() => navigation.goBack()));
  };
  const changeStore = (key, value) => {
    dispatch(changeData({ key, value }));
  };
  const clear = () => {
    dispatch(clearFilter());
    dispatch(getSearchList(() => navigation.goBack()));
  };
  if (searchLoading) return <LoadingBlock />;
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <FilterWrapper>
          <FilterCatButton
            title={t("setType")}
            name={t("select")}
            choiceType="sorts"
            setter={(value) => changeStore("sort_ids", value)}
            chosenData={sort_ids}
          />
          <View style={{ marginTop: 15 }}>
            <FilterCatButton
              title={t("category")}
              name={t("select")}
              choiceType="category"
              setter={(value) => changeStore("category_ids", value)}
              chosenData={category_ids}
            />
          </View>
          <View style={{ marginTop: 15 }}>
            <FilterCatButton
              title={t("peculiarity")}
              name={t("select")}
              choiceType="types"
              setter={(value) => changeStore("type_ids", value)}
              chosenData={type_ids}
            />
          </View>
          <View style={{ marginTop: 20 }}>
            <SemiBold14 color={global.colors.main}>{t("deposit")}</SemiBold14>
            <FilterInputWrap>
              <TextInput
                style={{
                  paddingHorizontal: 15,
                  alignItems: "center",
                  width: "50%",
                  height: 40,
                  borderStyle: "solid",
                  borderRightColor: "#efefef",
                  borderRightWidth: 1,
                }}
                keyboardType={"numeric"}
                returnKeyType={"done"}
                value={from}
                onChangeText={(text) => changeStore("from", text)}
                placeholder={t("from")}
                placeholderTextColor={global.colors.textColor2}
              />
              <TextInput
                style={{
                  paddingHorizontal: 15,
                  alignItems: "center",
                  width: "50%",
                  height: 40,
                }}
                returnKeyType={"done"}
                keyboardType={"numeric"}
                value={to}
                onChangeText={(text) => changeStore("to", text)}
                placeholder={t("to1")}
                placeholderTextColor={global.colors.textColor2}
              />
            </FilterInputWrap>
          </View>
        </FilterWrapper>
      </ScrollView>
      <FilterButtonsWrap>
        <FilterButton onPress={clear}>
          <SemiBold13>{t("reset")}</SemiBold13>
        </FilterButton>
        <FilterButton onPress={makeSearch} color={global.colors.lightGreen}>
          <SemiBold13>{t("get")}</SemiBold13>
        </FilterButton>
      </FilterButtonsWrap>
    </>
  );
}
