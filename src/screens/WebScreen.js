import React, { useEffect } from "react";
import { WebView } from "react-native-webview";
import { TabActions, useNavigation } from "@react-navigation/native";

export default function WebScreen({ route }) {
  const { url } = route.params;
  const navigation = useNavigation();
  const navigateToProfile = () => {
    const jumpToAction = TabActions.jumpTo("Profile");
    navigation.dispatch(jumpToAction);
    navigation.navigate("MyOrdersScreen");
  };
  return (
    <WebView
      source={{ uri: url }}
      onNavigationStateChange={(navState) => {
        if (navState.url == "https://kayvoni.uz") {
          navigateToProfile();
        }
      }}
    />
  );
}
