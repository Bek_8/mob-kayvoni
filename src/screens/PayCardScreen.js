import React, { useState } from "react";
import { View, Image, ScrollView } from "react-native";
import global from "../../resources/global";
import { Book13, SemiBold13 } from "../../resources/palettes";
import { TextInputMask } from "react-native-masked-text";
import { DateWrap, PayButton } from "../../resources/styles";
import { useNavigation } from "@react-navigation/native";

export default function PayCardScreen() {
  const navigation = useNavigation();
  const [phone, setPhone] = useState("");
  return (
    <>
      <ScrollView
        style={{
          padding: 10,
          backgroundColor: global.colors.white,
        }}
      >
        <View
          style={{
            justifyContent: "center",
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 110,
            }}
          >
            <Image
              style={{ width: 145, height: 145 }}
              source={global.images.payMe}
            />
          </View>
          <View style={{ marginTop: 75 }}>
            <Book13 style={{ marginLeft: 10 }} color={global.colors.main}>
              Номер карточки:
            </Book13>
            <DateWrap>
              <TextInputMask
                type={`credit-card`}
                options={{
                  mask: "0000 0000 0000 0000",
                }}
                keyboardType={`phone-pad`}
                returnKeyType={"done"}
                value={phone}
                onChangeText={(text) => setPhone(text)}
                maxLength={19}
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: global.colors.white,
                  marginTop: 10,
                  borderRadius: 10,
                  padding: 10,
                  textAlign: "center",
                }}
              />
            </DateWrap>
          </View>
        </View>
      </ScrollView>
      <PayButton onPress={() => navigation.navigate("PaidScreen")}>
        <SemiBold13>Оплатить</SemiBold13>
      </PayButton>
    </>
  );
}
