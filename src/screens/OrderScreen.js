import React, { useEffect } from "react";
import { ScrollView, Image, View } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import { Book14, Bold18, Bold12, Bold14 } from "../../resources/palettes";
import StatusBlock from "../components/StatusBlock";
import OrderFoodMenu from "../components/OrderFoodMenu";
import CheckBlock from "../components/CheckBlock";
import { useDispatch, useSelector } from "react-redux";
import { getUserOrderById } from "../redux/actions/userOrdersAction";
import { handleStatusColor, StatusName } from "../redux/utils";
import LoadingScreen from "./LoadingScreen";
import { useTranslation } from "react-i18next";
import MoveDriveButton from "../components/DriveButton";
export default function OrderScreen({ navigation, route }) {
  const { t, i18n } = useTranslation();
  const current_lang = i18n.language;
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);
  const { userOrder, userOrdersLoading, userOrdersError } = useSelector(
    (state) => state.userOrders
  );
  const { id } = route.params;
  useEffect(() => {
    dispatch(getUserOrderById(token, id));
  }, []);
  const status = 1;
  const handleStatusInfo = (status) => {
    if (status == 1) return false;
    if (status == 2 || status == 3) {
      return (
        <StatusBlock
          text={t("tabelNumber")}
          status={23}
          bgColor={global.colors.white}
          nameColor={global.colors.textColor}
        />
      );
    }
    if (status == -2) {
      return (
        <TextArea>
          <Bold14 color={global.colors.textColor}>{t("reason")}</Bold14>
        </TextArea>
      );
    }
  };
  const handleStatusCheck = (status) => {
    if (status == -2)
      return (
        <CheckBlock name={t("returnCash")} price={userOrder.total_price} />
      );
  };
  if (userOrdersError) return <Text>{userOrdersError}</Text>;
  if (userOrdersLoading || !userOrder) return <LoadingScreen />;
  return (
    <ScrollView style={{ backgroundColor: global.colors.white }}>
      <OrderWrap>
        <Book14 color={global.colors.textColor2}>
          {userOrder.reserve_time}
        </Book14>
        <TitleWrap>
          <Bold18 style={{ flex: 1 }} color={global.colors.textColor}>
            {userOrder.restaurant.name}
          </Bold18>
          <Image
            style={{ width: 40, height: 40, borderRadius: 40 }}
            source={{ uri: userOrder.restaurant.logo }}
          />
        </TitleWrap>
        <MoveDriveButton
          lat={userOrder.restaurant.lat}
          long={userOrder.restaurant.long}
          name={userOrder.restaurant.name}
        />
        <StatusBlock
          text={t("status")}
          bgColor={handleStatusColor(userOrder.status)}
          status={<StatusName status={userOrder.status} />}
        />
        {handleStatusInfo(status)}
        {userOrder.comment && (
          <>
            <Bold12 style={{ marginTop: 10 }} color={global.colors.main}>
              {t("preference")}
            </Bold12>
            <StatusBlock
              text={userOrder.comment}
              bgColor={global.colors.main}
            />
          </>
        )}
        {userOrder.order_item.map((item, key) => (
          <OrderFoodMenu
            key={`${key} ${item.id}`}
            count={item.count}
            name={item.food[`name_${current_lang}`]}
            sort={item.params[`name_${current_lang}`]}
            price={item.total_price}
          />
        ))}
        <View style={{ marginTop: 10 }}>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: global.colors.textColor2,
              paddingVertical: 10,
            }}
          >
            <CheckBlock name={t("orderCost")} price={userOrder.real_price} />
            <CheckBlock
              name={t("persons")}
              price={`(x${userOrder.seats_count}) ${userOrder.seats_price}`}
            />
          </View>
          <CheckBlock name={t("paid")} price={userOrder.payed_amount} />
          {handleStatusCheck()}
        </View>
      </OrderWrap>
    </ScrollView>
  );
}

const OrderWrap = styled.View`
  padding: 10px;
`;

const TitleWrap = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const TextArea = styled.View`
  padding: 10px;
  margin-top: 10px;
  flex: 1;
  height: 150px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 20px;
`;
