import React, { useEffect } from "react";
import { ScrollView, Text } from "react-native";
import global from "../../resources/global";
import styled from "styled-components";
import FullCategory from "../components/FullCategory";
import RestaurantBlock from "../components/RestaurantBlock";
import { RestCategoryWrap } from "../../resources/styles";
import { useRoute } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { getCategoryById } from "../redux/actions/categoryAction";
import LoadingBlock from "../components/LoadingBlock";

export default function RestCategory() {
  const route = useRoute();
  const { id, name, color, image } = route.params;

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategoryById(id));
  }, []);
  const {
    categoryScreen,
    categoryScreenLoading,
    categoryScreenError,
  } = useSelector((state) => state.category);
  if (categoryScreenLoading) return <LoadingBlock />;
  if (categoryScreenError) return <Text>{categoryScreenError.message}</Text>;
  return (
    <ScrollView style={{ backgroundColor: global.colors.white, padding: 10 }}>
      <FullCategory name={name} color={color} image={image} />

      <RestCategoryWrap>
        {!!categoryScreen.length &&
          categoryScreen.map((rest) => {
            return (
              <RestaurantBlock
                key={`${rest.id} ${rest.name}`}
                restaurant={rest}
              />
            );
          })}
        {/* <RestaurantBlock
          image={global.images.yaponaMama}
          name="Yapona mama Yapona mama"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.wok}
          name="Wok Wok Wok Wok"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.yaponaMama}
          name="Yapona mama Yapona mama"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.wok}
          name="Wok Wok Wok Wok"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.yaponaMama}
          name="Yapona mama Yapona mama"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.wok}
          name="Wok Wok Wok Wok"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.yaponaMama}
          name="Yapona mama Yapona mama"
          sort="Азиатская еда"
        />
        <RestaurantBlock
          image={global.images.wok}
          name="Wok Wok Wok Wok"
          sort="Азиатская еда"
        /> */}
      </RestCategoryWrap>
    </ScrollView>
  );
}
