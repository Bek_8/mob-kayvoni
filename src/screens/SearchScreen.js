import React, { useEffect, useState } from "react";
import { View, TextInput, ScrollView, Text, Image } from "react-native";
import global from "../../resources/global";
import { SemiBold14 } from "../../resources/palettes";
import {
  RestaurantWrap,
  SearchWrap,
  SearchInputWrap,
  SearchInput,
  FilterWrap,
  SearchButtonsWrap,
  SearchButton,
  MapButton,
} from "../../resources/styles";
import FilterIcon from "../../assets/img/filterIcon.svg";
import SearchFilter from "../../assets/img/searchFilter.svg";
import FoodBlock from "../components/FoodBlock";
import RestaurantBlock from "../components/RestaurantBlock";
import Map from "../../assets/img/map.svg";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { getSearchList, changeData } from "../redux/actions/searchAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

const FoodItems = ({ foods = [] }) => (
  <View style={{ marginTop: 10 }}>
    {foods &&
      foods.map((food, key) => {
        return <FoodBlock food={food} key={`${food.id}${key}`} />;
      })}
  </View>
);
const RestaurantItems = ({ navigation, restaurant = [], title = "" }) => (
  <>
    <MapButton onPress={() => navigation.navigate("MapScreen")}>
      <Map />
      <SemiBold14 style={{ marginLeft: 10 }}>{title}</SemiBold14>
    </MapButton>
    <View height={25} />
    <RestaurantWrap>
      {restaurant &&
        restaurant.map((rest, key) => {
          return <RestaurantBlock restaurant={rest} key={`${rest.id}${key}`} />;
        })}
    </RestaurantWrap>
  </>
);
export default function SearchScreen() {
  const { t } = useTranslation();
  const [type, setType] = useState(1);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { search, keyword, searchLoading, searchError } = useSelector(
    (state) => state.search
  );
  const food = search ? search.food : [];
  const restaurant = search ? search.restaurants : [];
  useEffect(() => {
    searchRequest();
  }, []);
  const searchRequest = () => {
    dispatch(getSearchList());
  };
  if (searchLoading) return <LoadingBlock />;
  if (searchError) return <Text>{searchError.message}</Text>;
  const changeStore = (key, value) => {
    dispatch(changeData({ key, value }));
  };
  return (
    <ScrollView style={{ backgroundColor: global.colors.white }}>
      <SearchWrap>
        <SearchInputWrap>
          <SearchInput>
            <SearchFilter />
            <TextInput
              value={keyword}
              onChangeText={(text) => changeStore("keyword", text)}
              onSubmitEditing={() => searchRequest()}
              style={{ width: "100%", height: 40, marginLeft: 10 }}
            />
          </SearchInput>
          <FilterWrap onPress={() => navigation.navigate("FilterScreen")}>
            <FilterIcon />
          </FilterWrap>
        </SearchInputWrap>
        <SearchButtonsWrap>
          <SearchButton
            color={type != 0 ? global.colors.white : global.colors.main}
            onPress={() => type != 0 && setType(0)}
          >
            <SemiBold14
              color={type != 0 ? global.colors.main : global.colors.white}
            >
              {t("food")}
            </SemiBold14>
          </SearchButton>
          <SearchButton
            onPress={() => type != 1 && setType(1)}
            color={type != 1 ? global.colors.white : global.colors.main}
          >
            <SemiBold14
              color={type != 1 ? global.colors.main : global.colors.white}
            >
              {t("restaurants")}
            </SemiBold14>
          </SearchButton>
        </SearchButtonsWrap>
        {type ? (
          <RestaurantItems
            title={t("onMap")}
            navigation={navigation}
            restaurant={restaurant}
          />
        ) : (
          <FoodItems foods={food} />
        )}
      </SearchWrap>
    </ScrollView>
  );
}
