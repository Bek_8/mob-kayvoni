class ApiService {
  static _host = "http://kayvoni.uz";
  static _apiBase = this._host + "/api";
  static _headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  static postEvent = async (url, token = null, body) => {
    let headers = this._headers;
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(body),
    }).then((res) => {
      return res.json();
    });
  };
  static patchEvent = async (url, token = null, body) => {
    let headers = this._headers;
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    const res = await fetch(this._apiBase + url, {
      method: "PATCH",
      headers: headers,
      body: JSON.stringify(body),
    });
    return res.json();
  };

  static updateEvent = async (url, token = null, form) => {
    let headers = this._headers;
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      return res.json();
    });
  };
  static getResources = async (url, token = null) => {
    let headers = this._headers;

    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }

    const res = await fetch(this._apiBase + url, { headers });
    return await res.json();
  };
}

export default ApiService;
